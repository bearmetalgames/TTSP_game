extends MoverAndShooter

@export var seconds_between_shots:float = 1.0

# Spawn points for bullets
@export var cannon:Node2D

var auto_direction = Vector2(1.0, 0.0)
var time_since_shot: float = 0

func _ready():
	super._ready()

	bullet_prefab = preload("res://prefabs/bullet_enemy.tscn")
	cannon = get_node("FrontCannon") as Node2D
	# Just setting this since this aprite is a subsection of a sprite sheet
	sprite_width = sprite.get_rect().size.x as int

func get_direction_with_bounds():
	# # Check y bounds
	# if direction.y > 0 and global_position.y >= screen_bounds.y:
	# 	direction.y = 0.0

	# if direction.y < 0 and global_position.y <= sprite_width:
	# 	direction.y = 0.0
	
	# Why do I need to multiply screen x by 2?????
	if global_position.x >= screen_bounds.x * 2 or global_position.x <= sprite_width:
		auto_direction.x *= -1.0

	return auto_direction

func process_collision(collision_info):
	var collider = collision_info.get_collider()

func process_shot(delta):
	time_since_shot += delta

	if time_since_shot >= seconds_between_shots:
		var new_bullet = bullet_prefab.instantiate() as CharacterBody2D
		new_bullet.transform = cannon.global_transform

		# add_child(new_bullet)
		owner.add_child(new_bullet)
		time_since_shot = 0.0
