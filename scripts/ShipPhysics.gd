extends MoverAndShooter


# Spawn points for bullets
@export var left_cannon:Node2D
@export var right_cannon:Node2D

# Time ignition needs to be held for restart of engine
@export var ignition_time:float = 3.0

# Every second, this amount is taken away from the gun heat
@export var cool_down_time_speed:float = 0.04

# The time it takes for the full bar to cool down
@export var cool_down_time:float = 5.0

var shoot_from_right:bool = true

var right_engine_damaged:bool = false
var left_engine_damaged:bool = false

var left_hold_time:float = 0.0
var right_hold_time:float = 0.0

const SPRITE_WIDTH = 25 # Make this pulled from the sprite

var needs_pumped: bool = true
var on_down_stroke: bool = true
var num_pumps:int = 0

var gun_heat: float = 0.0
var can_shoot: bool = true


# TODO: Make a basic "character mover" that has some of the shared things between the enemy and player
func _ready():
	super._ready()
	left_cannon = get_node("LeftCannon") as Node2D
	right_cannon = get_node("RightCannon") as Node2D

func _process(delta):
	process_ignitor(delta)
	process_pump()


func _physics_process(delta):
	super._physics_process(delta)

func process_pump():
	if needs_pumped:
		var down_stroke = Input.is_action_just_pressed("pump_down_stroke")
		var up_stroke = Input.is_action_just_pressed("pump_up_stroke")

		# if I'm on the down stroke, I am waiting for the up stroke
		if on_down_stroke:
			if up_stroke:
				num_pumps += 1
				on_down_stroke = !on_down_stroke

				# if you can't shoot, that means the gun is overheated
				if not can_shoot:
					gun_heat -= 0.1
					if gun_heat <= 0:
						gun_heat = 0
						can_shoot = true
		else:
			if down_stroke:
				on_down_stroke = !on_down_stroke
		



func process_ignitor(delta):
	if left_engine_damaged:
		var pressed = Input.is_action_pressed("left_engine_ignitor")
		if pressed:
			left_hold_time += delta
			if left_hold_time >= ignition_time:
				left_engine_damaged = false
				print("LEFT ENGINE RESTARTED")
		else:
			left_hold_time = 0
	
	if right_engine_damaged:
		var pressed = Input.is_action_pressed("right_engine_ignitor")
		if pressed:
			right_hold_time += delta
			if right_hold_time >= ignition_time:
				right_engine_damaged = false
				print("RIGHT ENGINE RESTARTED")
		else:
			right_hold_time = 0

# I don't think it is a bug, but there is behavior I don't like
# The collision is only processed whe movement happens.
# So, this function is only called if the ship moves in to a collider
# if another collider moves in to it while this one is still, this isn't called
# I really don't like that.
func process_collision(collision_info):
	var collided_obj = collision_info.get_collider() as CharacterBody2D
	if collided_obj.collision_layer == 8:
		var bullet = collided_obj as Bullet

		if position.x < bullet.position.x:
			# Damage right engine
			print("right side damaged")
			right_engine_damaged = true

		if position.x > bullet.position.x:
			# Damage left engine
			print("left side damaged")
			left_engine_damaged = true
		
		bullet.destroy()

func damage(amount):
	super.damage(amount)
	print("took damage: " + str(health))

func get_direction_with_bounds():
	var direction = Input.get_vector("ship_left", "ship_right", "ship_up", "ship_down")
	
	# Check y bounds
	if direction.y > 0 and global_position.y >= screen_bounds.y:
		direction.y = 0.0
	
	if direction.y < 0 and global_position.y <= sprite_width:
		direction.y = 0.0
		
	# Check x bounds, moving right
	if direction.x > 0:
		if right_engine_damaged:
			direction.x *= 0.5

		if global_position.x >= screen_bounds.x:
			direction.x = 0.0
			
	
	# Check x bounds, moving left
	if direction.x < 0:
		if left_engine_damaged:
			direction.x *= 0.5

		if global_position.x <= sprite_width:
			direction.x = 0.0
	
	return direction

func process_shot(delta):
	var basic_shot = Input.is_action_just_pressed("ship_basic_shot")
	if can_shoot:
		if basic_shot:
			gun_heat += 0.02
			var new_bullet = bullet_prefab.instantiate() as CharacterBody2D
			if shoot_from_right:
				new_bullet.transform = right_cannon.global_transform
			else:
				new_bullet.transform = left_cannon.global_transform
			owner.add_child(new_bullet)
			shoot_from_right = !shoot_from_right
		elif gun_heat > 0:
			gun_heat -= cool_down_time_speed * delta
			# do cooldown stuff
		if gun_heat >= 1:
			gun_heat = 1.0
			can_shoot = false
		
		

