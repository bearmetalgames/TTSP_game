class_name MoverAndShooter
extends CharacterBody2D

@export var movement_speed:float = 200.0
@export var sprite:Sprite2D
@export var health:int = 10;

var screen_bounds:Vector2
var bullet_prefab:PackedScene
var sprite_width:int = 25 # Make this pulled from the sprite

# Get the gravity from the project settings to be synced with RigidBody nodes.
var gravity = ProjectSettings.get_setting("physics/2d/default_gravity")


# TODO: Make a basic "character mover" that has some of the shared things between the enemy and player
func _ready():
	var screen_size = get_viewport_rect().size
	sprite = get_node("Sprite2D") as Sprite2D
	sprite_width = sprite.texture.get_width()
	screen_bounds.x = screen_size.x - sprite_width
	screen_bounds.y = screen_size.y - sprite_width
	bullet_prefab = preload("res://prefabs/bullet.tscn")

# Can be filled in by child. Defaults to player movement.
func get_direction_with_bounds():
	var direction = Input.get_vector("ship_left", "ship_right", "ship_up", "ship_down")

	# Check y bounds
	if direction.y > 0 and global_position.y >= screen_bounds.y:
		direction.y = 0.0

	if direction.y < 0 and global_position.y <= sprite_width:
		direction.y = 0.0
	
	# Check x bounds
	if direction.x > 0 and global_position.x >= screen_bounds.x:
		direction.x = 0.0

	if direction.x < 0 and global_position.x <= sprite_width:
		direction.x = 0.0

	return direction

# Should be filled in by child
func process_shot(delta):
	pass

func damage(amount):
	health -= amount;

func heal(amount):
	health += amount;

func process_collision(collision_info):
	var collider = collision_info.get_collider()

func _physics_process(delta):
	# Add the gravity.
	# if not is_on_floor():
	# 	velocity.y += gravity * delta

	process_shot(delta)

	# Get the input direction and handle the movement/deceleration.
	# As good practice, you should replace UI actions with custom gameplay actions.
	var direction = get_direction_with_bounds()
	velocity = direction * movement_speed

	var collision_info = move_and_collide(velocity * delta)
	if collision_info:
		process_collision(collision_info)

