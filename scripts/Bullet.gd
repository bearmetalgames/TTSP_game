class_name Bullet
extends CharacterBody2D


@export var shot_speed:float = 300.0
@export var sprite:Sprite2D
@export var direction:int = -1

func _ready():
	sprite = get_node("Sprite2D")

func check_bounds():
	if position.y < -100 or position.y > 2000:
		queue_free()

func destroy():
	queue_free()

func _physics_process(delta):
	check_bounds()

	# Move the shot up
	velocity.y = direction * shot_speed


	var collision_info = move_and_collide(velocity * delta)
	if collision_info:
		var collider = collision_info.get_collider()
		if collider.collision_layer == 1 or collider.collision_layer == 4:
			var damageable = collider as MoverAndShooter
			damageable.damage(1)
			
		queue_free()

